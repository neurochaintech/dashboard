from ubuntu:18.04

WORKDIR /app
COPY bot_graph.py .

RUN apt update
RUN apt install -y python3-flask

EXPOSE 80

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV FLASK_APP=bot_graph.py
CMD flask run --host 0.0.0.0 -p 80
