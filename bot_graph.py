import json

from flask import Flask, request, render_template_string

app = Flask(__name__)

template = """
<html>
<head>
<meta charset="utf-8">
<style>

.link {
  fill: none;
  stroke: #666;
  stroke-width: 1.5px;
}

#licensing {
  fill: green;
}

.link.licensing {
  stroke: green;
}

.link.resolved {
  stroke-dasharray: 0,2 1;
}

circle {
  fill: #ccc;
  stroke: #333;
  stroke-width: 1.5px;
}

text {
  font: 10px sans-serif;
  pointer-events: none;
  text-shadow: 0 1px 0 #fff, 1px 0 0 #fff, 0 -1px 0 #fff, -1px 0 0 #fff;
}

</style>
</head>
<body>


<script src="//d3js.org/d3.v3.min.js"></script>
<script>

var links = {{ edges | safe }};

var nodes = {};

// Compute the distinct nodes from the links.
links.forEach(function(link) {
link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
});

var width = 800,
    height = 800;

var force = d3.layout.force()
    .nodes(d3.values(nodes))
    .links(links)
    .size([width, height])
    .linkDistance(20)
    .charge(-300)
    .on("tick", tick)
    .start();

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("style", "float:left");

// Per-type markers, as they don't inherit styles.
svg.append("defs").selectAll("marker")
    .data(["suit", "licensing", "resolved"])
  .enter().append("marker")
    .attr("id", function(d) { return d; })
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 15)
    .attr("refY", -1.5)
    .attr("markerWidth", 6)
    .attr("markerHeight", 6)
    .attr("orient", "auto")
  .append("path")
    .attr("d", "M0,-5L10,0L0,5");

var path = svg.append("g").selectAll("path")
    .data(force.links())
  .enter().append("path")
    .attr("class", function(d) { return "link " + d.type; })
    .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });

var circle = svg.append("g").selectAll("circle")
    .data(force.nodes())
  .enter().append("circle")
    .attr("r", 6)
    .call(force.drag);

var text = svg.append("g").selectAll("text")
    .data(force.nodes())
  .enter().append("text")
    .attr("x", 8)
    .attr("y", ".31em")
    .text(function(d) { return d.name; });

// Use elliptical arc path segments to doubly-encode directionality.
function tick() {
  path.attr("d", linkArc);
  circle.attr("transform", transform);
  text.attr("transform", transform);
}

function linkArc(d) {
  var dx = d.target.x - d.source.x,
      dy = d.target.y - d.source.y,
      dr = Math.sqrt(dx * dx + dy * dy);
  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 "
  + d.target.x + "," + d.target.y;
}

function transform(d) {
  return "translate(" + d.x + "," + d.y + ")";
}

<!--setTimeout(function(){-->
   <!--window.location.reload(1);-->
<!--}, 5000);-->

</script>

<div id="graph" width="800" height="800" style="float:left"></div>
{% for iframe in iframes %}
  <iframe height="400" width="800" src="{{iframe}}"></iframe>
{% endfor %}

</body>
</html>
"""

IFRAMES = [
    "https://dashboard.neurochaintech.io/d/rjKBoo5mk/bot-ut?"
    "orgId=1&panelId=4&fullscreen&from=1538749160897&to=1539958760897",
    "https://dashboard.neurochaintech.io/d/rjKBoo5mk/bot-ut?"
    "orgId=1&panelId=2&fullscreen&from=1538750031437&to=1539959631437",
]

NAME_SIZE = 5

graph = {}


def get_edges(graph):
    edges = []
    for x, ys in graph.items():
        for y in ys:
            edges.append([x, y])

    return json.dumps(
        [
            {
                "source": str(a)[:NAME_SIZE],
                "target": str(b)[:NAME_SIZE],
                "type": "suit",
            }
            for a, b in edges
        ]
    )


@app.route("/", methods=["POST"])
def receive_connections():
    connections = json.loads(request.data)
    graph[connections["ownAddress"]["data"]] = [
        address["data"] for address in connections["peersAddresses"]
    ]
    return ""


@app.route("/", methods=["GET"])
def index():
    return render_template_string(
        template, edges=get_edges(graph), iframes=IFRAMES
    )
